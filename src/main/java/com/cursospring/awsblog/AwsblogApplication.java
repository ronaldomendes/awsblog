package com.cursospring.awsblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

//@SpringBootApplication(exclude = { SecurityAutoConfiguration.class }) // bloqueia o Spring Security
@SpringBootApplication
public class AwsblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsblogApplication.class, args);
	}
}
