package com.cursospring.awsblog.utils;

import com.cursospring.awsblog.model.Post;
import com.cursospring.awsblog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DummyData {

    PostRepository repository;

    @Autowired
    public void setRepository(PostRepository repository) {
        this.repository = repository;
    }

//    @PostConstruct
    public void savePosts() {
        List<Post> postList = new ArrayList<>();
        Post post01 = new Post();
        post01.setAutor("Godofredo Santos");
        post01.setTitulo("Spring Boot");
        post01.setData(LocalDate.now());
        post01.setTexto("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus nulla, bibendum quis " +
                "dapibus eu, blandit at urna. Aenean placerat porttitor libero a scelerisque. Donec euismod, justo " +
                "ut gravida aliquam, nulla nisl ultrices magna, quis suscipit urna tellus et odio. Nullam ornare " +
                "egestas eros, vitae venenatis sapien facilisis vitae. Phasellus pharetra orci eget pellentesque " +
                "bibendum. Duis non malesuada sem. Proin eleifend dictum turpis. Proin vel augue magna.");

        Post post02 = new Post();
        post02.setAutor("Creuza Silva");
        post02.setTitulo("Spring Boot");
        post02.setData(LocalDate.now());
        post02.setTexto("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus nulla, bibendum quis " +
                "dapibus eu, blandit at urna. Aenean placerat porttitor libero a scelerisque. Donec euismod, justo " +
                "ut gravida aliquam, nulla nisl ultrices magna, quis suscipit urna tellus et odio. Nullam ornare " +
                "egestas eros, vitae venenatis sapien facilisis vitae. Phasellus pharetra orci eget pellentesque " +
                "bibendum. Duis non malesuada sem. Proin eleifend dictum turpis. Proin vel augue magna.");

        postList.add(post01);
        postList.add(post02);

        for (Post post : postList) {
            Post postSaved = repository.save(post);
            System.out.println(postSaved.getId());
        }
    }
}
