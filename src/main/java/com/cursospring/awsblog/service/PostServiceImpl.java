package com.cursospring.awsblog.service;

import com.cursospring.awsblog.model.Post;
import com.cursospring.awsblog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository repository;

    @Autowired
    public void setRepository(PostRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Post> findAll() {
        return repository.findAll().stream().sorted(Comparator.comparing(Post::getId).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public Post findById(Long id) {
        return repository.findById(id).orElseThrow(null);
    }

    @Override
    public Post save(Post post) {
        return repository.save(post);
    }
}
