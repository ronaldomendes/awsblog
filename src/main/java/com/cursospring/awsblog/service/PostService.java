package com.cursospring.awsblog.service;

import com.cursospring.awsblog.model.Post;

import java.util.List;

public interface PostService {
    List<Post> findAll();
    Post findById(Long id);
    Post save(Post post);
}
